Tweakster
=========

**WARNING: This project is in early prototyping phase, I'd advise
against trying to do anything meaningful with it**

`tweakster` is a utility aimed at managing device-specific tweaks.

It relies on per-device TOML config files and copies/links the
appropriate configuration files/fragments for those tweaks to be
applied.

Example configurations and files can be found
[here](https://salsa.debian.org/Mobian-team/tweaks-data).

Finally, this readme should really be improved. But for now, this
will do.

*Note: this project is as much a needed piece of software as it is
a learning experience, aiming at getting more familiar with Rust.
In this regard, comments and suggestions for improving the code
quality and readability are welcome.*

## License

`tweakster` is licensed under the terms of the
[MIT license](https://spdx.org/licenses/MIT.html).

## Contributing

Feel free to open issues and/or merge requests on the project's
[gitlab](https://gitlab.com/mobian1/tweakster) repo.
