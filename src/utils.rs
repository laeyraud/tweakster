use anyhow::{anyhow, ensure, Result};
use merge::Merge;
use serde::Deserialize;
use std::io::Write;
use std::process::Command;
use std::{fs, io, os::unix, path::PathBuf};

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct FilesArray {
    #[merge(strategy = merge::vec::append)]
    pub files: Vec<String>,
}

#[derive(Deserialize, Debug, Default, Clone)]
pub struct Link {
    pub origin_dir: PathBuf,
    pub origin_file: PathBuf,
    pub destination_dir: PathBuf,
    pub destination_file: PathBuf,
}

pub fn execute(command: &str, arguments: Option<Vec<&str>>) -> Result<()> {
    let mut exe = Command::new(command);

    if let Some(args) = arguments {
        exe.args(args);
    }

    let res = exe.output();
    match res {
        Ok(output) => {
            println!("{}: command status: {}", command, output.status);
            io::stdout().write_all(&output.stdout).unwrap();
            io::stderr().write_all(&output.stderr).unwrap();
            Ok(())
        }
        Err(error) => {
            println!("{}: unable to execute command!", command);
            Err(anyhow!(error))
        }
    }
}

pub fn stringlist_to_links(
    strlist: &Vec<String>,
    origin: &PathBuf,
    destination: &PathBuf,
) -> Vec<Link> {
    let mut links: Vec<Link> = Vec::with_capacity(strlist.len());

    for file in strlist {
        let link = Link {
            origin_dir: PathBuf::from(origin),
            origin_file: PathBuf::from(&file),
            destination_dir: PathBuf::from(destination),
            destination_file: PathBuf::from(file),
        };

        links.push(link);
    }

    links
}

pub fn filelist_to_links(
    filelist: &FilesArray,
    origin: &PathBuf,
    destination: &PathBuf,
) -> Vec<Link> {
    stringlist_to_links(&filelist.files, origin, destination)
}

pub fn link_files(links: &Vec<Link>) -> Result<()> {
    ensure!(!links.is_empty(), "Link list is empty");

    for link in links {
        if !link.destination_dir.exists() {
            fs::create_dir_all(&link.destination_dir)?;
        }

        let origin = link.origin_dir.join(link.origin_file.clone());
        let destination = link.destination_dir.join(link.destination_file.clone());

        if fs::read_link(&destination).is_err() {
            unix::fs::symlink(origin, destination)?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{fs::OpenOptions, io::Write, path::Path};
    use tempfile::{tempdir, TempDir};

    fn helper_create_file(file_path: &Path, content: &[u8]) -> Result<()> {
        // Create and open an empty, writable file
        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(file_path)?;

        // Set file contents using function args
        file.write_all(content)?;

        Ok(())
    }

    #[test]
    fn execute_nominal_with_args() {
        let result = execute("echo", Some(vec!["foo", "bar"]));
        assert!(result.is_ok());
    }

    #[test]
    fn execute_nominal_no_arg() {
        let result = execute("echo", None);
        assert!(result.is_ok());
    }

    #[test]
    fn execute_error_not_found() {
        // Call with non-existing command and expect an error
        let result = execute("echo99", None);
        assert!(result.is_err());
    }

    #[test]
    fn stringlist_to_links_nominal() {
        let strlist: Vec<String> = vec!["file1".to_string(), "file2".to_string()];

        let src_dir = PathBuf::from("/foo");
        let dst_dir = PathBuf::from("/bar");

        let link_list: Vec<Link> = stringlist_to_links(&strlist, &src_dir, &dst_dir);
        assert_eq!(link_list.len(), strlist.len());

        for (index, elem) in strlist.iter().enumerate() {
            let link = &link_list[index];
            let expect_filename = std::ffi::OsString::from(&elem);

            assert_eq!(link.origin_file.file_name().unwrap(), expect_filename);
            assert_eq!(link.destination_file.file_name().unwrap(), expect_filename);
            assert_eq!(link.origin_dir, src_dir);
            assert_eq!(link.destination_dir, dst_dir);
        }
    }

    #[test]
    fn stringlist_to_links_error_empty() {
        let strlist: Vec<String> = vec![];

        let src_dir = PathBuf::from("/foo");
        let dst_dir = PathBuf::from("/bar");

        let link_list: Vec<Link> = stringlist_to_links(&strlist, &src_dir, &dst_dir);
        assert!(link_list.is_empty());
    }

    #[test]
    fn filelist_to_links_nominal() {
        let file_array = FilesArray {
            files: vec!["file1".to_string(), "file2".to_string()],
        };

        let src_dir = PathBuf::from("/foo");
        let dst_dir = PathBuf::from("/bar");

        let link_list: Vec<Link> = filelist_to_links(&file_array, &src_dir, &dst_dir);
        assert_eq!(link_list.len(), file_array.files.len());

        for (index, item) in file_array.files.iter().enumerate() {
            let link = &link_list[index];
            let expect_filename = std::ffi::OsString::from(&item);

            assert_eq!(link.origin_file.file_name().unwrap(), expect_filename);
            assert_eq!(link.destination_file.file_name().unwrap(), expect_filename);
            assert_eq!(link.origin_dir, src_dir);
            assert_eq!(link.destination_dir, dst_dir);
        }
    }

    #[test]
    fn link_files_nominal() {
        let nb_link = 4;
        let mut test_link_list: Vec<Link> = vec![];
        let mut test_tempdir_list: Vec<TempDir> = vec![];

        for link_index in 1..=nb_link {
            // Create a temp directory for origin and destination directories
            let src_temp_dir = tempdir().unwrap();
            let dst_temp_dir = tempdir().unwrap();
            let src_fname = format!("test_file_{}", link_index);
            let dst_fname = format!("test_link_{}", link_index);

            // Create a temp file in origin directory
            let src_fpath = src_temp_dir.path().join(src_fname.clone());
            let result = helper_create_file(src_fpath.as_path(), b"\n");
            assert!(result.is_ok());
            assert!(src_fpath.exists());

            // Create a Link structure with origin and destination info
            let link = Link {
                origin_dir: PathBuf::from(src_temp_dir.path()),
                origin_file: PathBuf::from(src_fname),
                destination_dir: PathBuf::from(dst_temp_dir.path()).join("subfolder"),
                destination_file: PathBuf::from(dst_fname),
            };

            // Insert link into list
            test_link_list.push(link);

            // Insert temp directories into cleanup list
            test_tempdir_list.push(src_temp_dir);
            test_tempdir_list.push(dst_temp_dir);
        }

        // Check that API call returns OK
        let result = link_files(&test_link_list);
        assert!(result.is_ok());

        // Check that each expected symlink exists
        for test_link in test_link_list.iter() {
            let dest_link_path = test_link
                .destination_dir
                .join(test_link.destination_file.clone());
            assert!(dest_link_path.is_symlink());
        }

        // Cleanup temp directories
        for dir in test_tempdir_list {
            let _ = dir.close();
        }
    }

    #[test]
    fn link_files_nominal_link_creation_skipped() {
        let mut test_link_list: Vec<Link> = vec![];

        // Create a temp directory for origin directory
        let temp_dir = tempdir().unwrap();
        let src_fname = "test_file".to_string();
        let dst_fname = "test_link".to_string();

        // Create a temp file in origin directory
        let src_fpath = temp_dir.path().join(src_fname.clone());
        let result = helper_create_file(src_fpath.as_path(), b"\n");
        assert!(result.is_ok());
        assert!(src_fpath.exists());

        // Create a Link structure with origin and destination info as identical
        let link = Link {
            origin_dir: PathBuf::from(temp_dir.path()),
            origin_file: PathBuf::from(src_fname.clone()),
            destination_dir: PathBuf::from(temp_dir.path()),
            destination_file: PathBuf::from(dst_fname.clone()),
        };

        // Create a symlink
        let result = unix::fs::symlink(
            temp_dir.path().join(src_fname),
            temp_dir.path().join(dst_fname),
        );
        assert!(result.is_ok());

        // Insert link into list
        test_link_list.push(link);

        // Check that API call returns ok
        let result = link_files(&test_link_list);
        assert!(result.is_ok());

        // Cleanup temp directory
        let _ = temp_dir.close();
    }

    #[test]
    fn link_files_error_link_creation() {
        let mut test_link_list: Vec<Link> = vec![];

        // Create a temp directory for origin directory
        let temp_dir = tempdir().unwrap();
        let fname = "test_file".to_string();

        // Create a temp file in origin directory
        let src_fpath = temp_dir.path().join(fname.clone());
        let result = helper_create_file(src_fpath.as_path(), b"\n");
        assert!(result.is_ok());
        assert!(src_fpath.exists());

        // Create a Link structure with origin and destination info as identical
        let link = Link {
            origin_dir: PathBuf::from(temp_dir.path()),
            origin_file: PathBuf::from(fname.clone()),
            destination_dir: PathBuf::from(temp_dir.path()),
            destination_file: PathBuf::from(fname.clone()),
        };

        // Insert link into list
        test_link_list.push(link);

        // Check that API call returns an error
        let result = link_files(&test_link_list);
        assert!(result.is_err());

        // Cleanup temp directory
        let _ = temp_dir.close();
    }

    #[test]
    fn link_files_error_empty() {
        // Call with empty list and expect an error
        let result = link_files(&vec![]);
        assert!(result.is_err());
    }
}
