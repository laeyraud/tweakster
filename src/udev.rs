use anyhow::Result;
use merge::Merge;
use serde::Deserialize;
use std::path::PathBuf;

use crate::utils;

#[derive(Deserialize, Debug, Default, Clone)]
struct UdevRule {
    name: String,
    priority: u32,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    #[merge(strategy = merge::vec::append)]
    rules: Vec<UdevRule>,
}

pub fn process(udev: Config, root: &PathBuf) -> Result<()> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/udev");

    let mut udev_dir = PathBuf::from(root);
    udev_dir.push("etc/udev/rules.d");

    let mut links: Vec<utils::Link> = Vec::with_capacity(udev.rules.len());

    for ref rule in udev.rules {
        links.push(utils::Link {
            origin_dir: PathBuf::from(&settings_dir),
            origin_file: PathBuf::from(format!("{}.rules", rule.name)),
            destination_dir: PathBuf::from(&udev_dir),
            destination_file: PathBuf::from(format!("{}-{}.rules", rule.priority, rule.name)),
        });
    }

    utils::link_files(&links)?;
    utils::execute("/usr/bin/udevadm", Some(vec!["control", "--reload"]))?;

    Ok(())
}
