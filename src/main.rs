mod default;
mod device_detector;
mod gsettings;
mod initramfs;
mod kernel;
mod profile;
mod pulse;
mod systemd;
mod uboot;
mod udev;
mod utils;

use anyhow::{anyhow, ensure, Result};
use clap::Parser;
use device_detector::DeviceDetector;
use merge::Merge;
use serde::Deserialize;
use std::{fs, path::PathBuf};

#[derive(Debug, Parser)]
#[command(about = "Utility for handling device-specific tweaks")]
struct Cli {
    /// Chroot to work into
    #[structopt(short, long)]
    chroot: Option<PathBuf>,

    /// Device type (default: auto-detect)
    #[structopt(short, long)]
    device: Option<String>,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct IncludeArray {
    #[merge(strategy = merge::vec::append)]
    pub include: Vec<String>,
}

#[derive(Deserialize, Debug, Default, Clone)]
struct Config {
    general: Option<IncludeArray>,
    default: Option<utils::FilesArray>,
    gsettings: Option<gsettings::Config>,
    initramfs: Option<initramfs::Config>,
    kernel: Option<kernel::Config>,
    profile: Option<utils::FilesArray>,
    pulse: Option<pulse::Config>,
    systemd: Option<systemd::Config>,
    uboot: Option<utils::FilesArray>,
    udev: Option<udev::Config>,
}

struct UserConfig {
    chroot_dir: PathBuf,
    config_dir: PathBuf,
    name: String,
}

fn check_args(args: Cli) -> Result<UserConfig> {
    let chroot_dir = match args.chroot {
        Some(path) => path,
        _ => PathBuf::from("/"),
    };

    // Check that a configuration directory exist inside chroot directory
    let config_dir = PathBuf::from(&chroot_dir).join("etc/mobile-tweaks/configs");

    let name = match args.device {
        Some(str) => str,
        _ => DeviceDetector::new(chroot_dir.clone())
            .find_config()
            .map_err(|_| anyhow!("Unable to autodetect device"))?,
    };

    // Create user configuration from args
    Ok(UserConfig {
        chroot_dir,
        config_dir,
        name,
    })
}

fn read_config_file(config_dir: &PathBuf, config_name: &str) -> Result<Config> {
    // Create configuration file path from input parameters
    let cfg_file_path = PathBuf::from(&config_dir).join(format!("{}.toml", config_name));

    // Read configuration file from filesystem
    let config_contents = fs::read_to_string(cfg_file_path)?;

    // Parse configuration file content as TOML
    let config: Config = toml::from_str(&config_contents)?;

    Ok(config)
}

fn parse_config_file(config_dir: &PathBuf, config_name: &str) -> Result<Config> {
    // Parse configuration file content as TOML
    let config: Config = read_config_file(config_dir, config_name)?;

    let mut out_config: Config = Default::default();

    if let Some(ref general) = config.general {
        for inc in &general.include {
            let mut comps = inc.as_str().split('.');

            let include_config = read_config_file(config_dir, comps.next().unwrap())?;

            match comps.next() {
                Some("default") => out_config.default = include_config.default,
                Some("gsettings") => out_config.gsettings = include_config.gsettings,
                Some("initramfs") => out_config.initramfs = include_config.initramfs,
                Some("kernel") => out_config.kernel = include_config.kernel,
                Some("profile") => out_config.profile = include_config.profile,
                Some("pulse") => out_config.pulse = include_config.pulse,
                Some("systemd") => out_config.systemd = include_config.systemd,
                Some("uboot") => out_config.uboot = include_config.uboot,
                Some("udev") => out_config.udev = include_config.udev,
                Some(s) => println!("Unrecognized section '{}'", s),
                None => println!("Error: no section"),
            }
        }
    }

    if let Some(ref mut default) = out_config.default {
        default.merge(config.default.unwrap_or_default());
    } else {
        out_config.default = config.default;
    }
    if let Some(ref mut gsettings) = out_config.gsettings {
        gsettings.merge(config.gsettings.unwrap_or_default());
    } else {
        out_config.gsettings = config.gsettings;
    }
    if let Some(ref mut profile) = out_config.profile {
        profile.merge(config.profile.unwrap_or_default());
    } else {
        out_config.profile = config.profile;
    }
    if let Some(ref mut initramfs) = out_config.initramfs {
        initramfs.merge(config.initramfs.unwrap_or_default());
    } else {
        out_config.initramfs = config.initramfs;
    }
    if let Some(ref mut kernel) = out_config.kernel {
        kernel.merge(config.kernel.unwrap_or_default());
    } else {
        out_config.kernel = config.kernel;
    }
    if let Some(ref mut pulse) = out_config.pulse {
        pulse.merge(config.pulse.unwrap_or_default());
    } else {
        out_config.pulse = config.pulse;
    }
    if let Some(ref mut systemd) = out_config.systemd {
        systemd.merge(config.systemd.unwrap_or_default());
    } else {
        out_config.systemd = config.systemd;
    }
    if let Some(ref mut uboot) = out_config.uboot {
        uboot.merge(config.uboot.unwrap_or_default());
    } else {
        out_config.uboot = config.uboot;
    }
    if let Some(ref mut udev) = out_config.udev {
        udev.merge(config.udev.unwrap_or_default());
    } else {
        out_config.udev = config.udev;
    }

    Ok(out_config)
}

fn process_config(user_config: &UserConfig, config_data: Config) -> Result<()> {
    if let Some(default) = config_data.default {
        default::process(default, &user_config.chroot_dir)?;
    }
    if let Some(gsettings) = config_data.gsettings {
        gsettings::process(gsettings, &user_config.chroot_dir)?;
    }
    if let Some(initramfs) = config_data.initramfs {
        initramfs::process(initramfs, &user_config.chroot_dir)?;
    }
    if let Some(kernel) = config_data.kernel {
        kernel::process(kernel, &user_config.chroot_dir)?;
    }
    if let Some(profile) = config_data.profile {
        profile::process(profile, &user_config.chroot_dir)?;
    }
    if let Some(pulse) = config_data.pulse {
        pulse::process(pulse, &user_config.chroot_dir)?;
    }
    if let Some(systemd) = config_data.systemd {
        systemd::process(systemd, &user_config.chroot_dir)?;
    }
    if let Some(uboot) = config_data.uboot {
        uboot::process(uboot, &user_config.chroot_dir)?;
    }
    if let Some(udev) = config_data.udev {
        udev::process(udev, &user_config.chroot_dir)?;
    }

    Ok(())
}

fn main() -> Result<()> {
    // Create user config from CLI args
    let user_config = check_args(Cli::parse())?;

    ensure!(
        user_config.chroot_dir.exists(),
        "chroot directory does not exist"
    );

    ensure!(
        user_config.config_dir.exists(),
        "configuration directory does not exist"
    );

    // Try to read and parse given or autodetected configuration file
    let config_data = parse_config_file(&user_config.config_dir, &user_config.name)?;

    // Process configuration data
    process_config(&user_config, config_data)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{fs::OpenOptions, io::Write, path::Path};
    use tempfile::tempdir;

    fn helper_create_file(file_path: &Path, content: &str) -> Result<()> {
        // Create and open an empty, writable file
        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(file_path)?;

        // Set file contents
        file.write_all(content.as_bytes())?;

        Ok(())
    }

    #[test]
    fn check_args_nominal() {
        let temp_dir = tempdir().unwrap();

        // Create config dir in temp dir
        let cfg_dir = temp_dir.path().join("etc/mobile-tweaks/configs");
        let result = fs::create_dir_all(cfg_dir.clone());
        assert!(result.is_ok());

        let cli_args = Cli {
            chroot: Some(PathBuf::from(temp_dir.path())),
            device: Some("fake_name".to_string()),
        };

        let result = check_args(cli_args);
        assert!(result.is_ok());

        let user = result.unwrap();
        assert_eq!(user.chroot_dir, temp_dir.path());
        assert_eq!(user.config_dir, cfg_dir);
        assert_eq!(user.name, "fake_name");

        let _ = temp_dir.close();
    }

    #[test]
    fn check_args_nominal_no_chroot() {
        let temp_dir = tempdir().unwrap();

        let cli_args = Cli {
            chroot: None,
            device: Some("fake_name".to_string()),
        };

        let result = check_args(cli_args);
        assert!(result.is_ok());

        let user = result.unwrap();
        assert_eq!(user.chroot_dir, PathBuf::from("/"));
        assert_eq!(user.config_dir, PathBuf::from("/etc/mobile-tweaks/configs"));
        assert_eq!(user.name, "fake_name");

        let _ = temp_dir.close();
    }

    #[test]
    fn read_config_file_nominal() {
        let temp_dir = tempdir().unwrap();
        let cfg_name = "test_file";
        let cfg_dir = PathBuf::from(temp_dir.path());
        let cfg_fpath = PathBuf::from(&cfg_dir).join(format!("{}.toml", cfg_name));

        let config_content = r#"
        [general]
        include = [
            "foo", 
            "bar"
        ]

        [default]
        files = [ "stuff" ]

        [gsettings]
        overrides = [ "ok" ]

        [profile]
        files = [ "something" ]

        [pulse]
        script = "do_nothing"
        config = { rates = [ 1, 2 ], format = "unknown" }

        [foo]
        bar = [ "test" ]

        [kernel]
        postinst = [ { name = "a", prefix = "e" } ]
        prerm = [ { name = "b", prefix = "f" } ]
        postrm = [ { name = "c", prefix = "g" } ]
        preinst = [ { name = "d", prefix = "h" } ]

        [systemd]
        system = [
            { service = "motiv", fragments = [ "yep.conf" ] }
        ]
        user = [
            { service = "lazy", fragments = [ "nope.conf" ] }
        ]
        enable = [ "motiv.service" ]

        [uboot]
        files = [ "ping" ]
        
        [initramfs]
        hooks = [ "pirate" ]
        scripts = [
            { stage = "first", name = "bill" },
            { stage = "second", name = "john" },
        ]

        [udev]
        rules = [
            { name = "rule-1", priority = 2 },
            { name = "rule-2", priority = 4 },
        ]
        "#;

        let result = helper_create_file(&cfg_fpath, config_content);
        assert!(result.is_ok());

        let result = read_config_file(&cfg_dir, cfg_name);
        assert!(result.is_ok());

        let _ = temp_dir.close();
    }

    #[test]
    fn read_config_file_error_not_a_toml() {
        let temp_dir = tempdir().unwrap();
        let cfg_name = "test_file";
        let cfg_dir = PathBuf::from(temp_dir.path());
        let cfg_fpath = PathBuf::from(&cfg_dir).join(format!("{}.toml", cfg_name));

        let result = helper_create_file(&cfg_fpath, "invalid toml");
        assert!(result.is_ok());

        let result = read_config_file(&cfg_dir, cfg_name);
        assert!(result.is_err());

        let _ = temp_dir.close();
    }

    #[test]
    fn read_config_file_error_not_found() {
        let temp_dir = tempdir().unwrap();
        let cfg_name = "test_file";
        let cfg_dir = PathBuf::from(temp_dir.path());

        let result = read_config_file(&cfg_dir, cfg_name);
        assert!(result.is_err());

        let _ = temp_dir.close();
    }
}
