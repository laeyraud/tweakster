use anyhow::Result;
use merge::Merge;
use serde::Deserialize;
use std::path::PathBuf;

use crate::utils;

#[derive(Deserialize, Debug, Default, Clone, Merge)]
struct SystemdRule {
    #[merge(skip)]
    service: String,
    #[merge(strategy = merge::vec::append)]
    fragments: Vec<String>,
}

#[derive(Deserialize, Debug, Default, Clone, Merge)]
pub struct Config {
    system: Option<Vec<SystemdRule>>,
    user: Option<Vec<SystemdRule>>,
    enable: Option<Vec<String>>,
}

fn process_services(rules: Vec<SystemdRule>, origdir: PathBuf, destdir: PathBuf) -> Result<()> {
    let mut links: Vec<utils::Link> = Vec::new();

    for rule in rules {
        let mut origin = PathBuf::from(&origdir);
        origin.push(&rule.service);
        origin.set_extension("service.d");

        let mut fragments_dir = PathBuf::from(&destdir);
        fragments_dir.push(&rule.service);
        fragments_dir.set_extension("service.d");

        let mut filelist = utils::FilesArray { files: Vec::new() };

        for fragment in rule.fragments {
            filelist.files.push(fragment);
        }

        links.extend(utils::filelist_to_links(&filelist, &origin, &fragments_dir));
    }

    utils::link_files(&links)
}

pub fn process(systemd: Config, root: &PathBuf) -> Result<()> {
    let mut settings_dir = PathBuf::from("/");
    settings_dir.push("etc/mobile-tweaks/systemd");

    let mut services_dir = PathBuf::from(root);
    services_dir.push("etc/systemd/");

    if let Some(rules) = systemd.system {
        let mut orig_dir = PathBuf::from(&settings_dir);
        orig_dir.push("system");

        let mut dest_dir = PathBuf::from(&services_dir);
        dest_dir.push("system");

        process_services(rules, orig_dir, dest_dir)?;
    }

    if let Some(rules) = systemd.user {
        let mut orig_dir = PathBuf::from(&settings_dir);
        orig_dir.push("user");

        let mut dest_dir = PathBuf::from(&services_dir);
        dest_dir.push("user");

        process_services(rules, orig_dir, dest_dir)?;
    }

    if let Some(services) = systemd.enable {
        for service in services {
            println!("systemd: enable {}", service);
            utils::execute(
                "/usr/bin/systemctl",
                Some(vec!["enable", "--now", service.as_str()]),
            )?;
        }
    }

    utils::execute("/usr/bin/systemctl", Some(vec!["daemon-reload"]))?;

    Ok(())
}
