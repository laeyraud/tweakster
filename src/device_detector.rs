use anyhow::{anyhow, Result};
use std::{fs, path::PathBuf};

pub struct DeviceDetector {
    root: std::path::PathBuf,
}

impl DeviceDetector {
    pub fn new(root: PathBuf) -> DeviceDetector {
        DeviceDetector { root }
    }

    fn read_compatible_info(&self) -> Result<Vec<String>> {
        let path = PathBuf::from(&self.root).join("proc/device-tree/compatible");
        let contents = fs::read_to_string(path)?;

        let compatibles: Vec<String> = contents
            .split('\0')
            .filter(|s| !s.is_empty())
            .map(|s| s.to_string())
            .collect();

        Ok(compatibles)
    }

    pub fn find_config(&self) -> Result<String> {
        let cfg_path = PathBuf::from(&self.root).join("etc/mobile-tweaks/configs");

        // Try to read compatible device info
        let compatible_devices = self.read_compatible_info()?;

        // Loop on each config file in the config directory
        for entry in fs::read_dir(cfg_path)? {
            let fname = entry?.file_name();

            // Loop on each compatible machine info and return the first match
            for value in compatible_devices.iter() {
                let full_name = String::from(value) + ".toml";
                if fname == full_name.as_str() {
                    return Ok(value.to_string());
                }
            }
        }

        Err(anyhow!(
            "Unable to detect a compatible configuration for the '{:?}' device",
            compatible_devices
        ))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{fs::OpenOptions, io::Write, path::Path};
    use tempfile::tempdir;

    fn helper_create_compatible_file(root_path: &Path, content: &[u8]) -> Result<()> {
        // Compute compatible file absolute path
        let compatibles_file_path = root_path.join("proc/device-tree/compatible");

        // Create device tree directory
        fs::create_dir_all(compatibles_file_path.clone().parent().unwrap())?;

        // Create and open an empty, writable file in it
        let mut file = OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(compatibles_file_path)?;

        // Set file contents with data passed to the function
        file.write_all(content)?;

        Ok(())
    }

    fn helper_create_config_files(root_path: &Path, config_list: Vec<&str>) -> Result<()> {
        // Compute configuration directory file path
        let config_dir_path = root_path.join("etc/mobile-tweaks/configs");

        // Create configuration directory
        fs::create_dir_all(&config_dir_path)?;

        for config_name in config_list {
            let mut config_file_path = config_dir_path.clone();
            config_file_path.push(config_name);
            config_file_path.set_extension("toml");

            // Create and open an empty, writable file in it
            let mut file = OpenOptions::new()
                .create(true)
                .truncate(true)
                .write(true)
                .open(config_file_path)?;

            file.write_all(b"\n")?;
        }

        Ok(())
    }

    #[test]
    fn read_compatible_info_nominal() {
        // Create a temp directory
        let temp_dir = tempdir().unwrap();

        // Create a device detector instance
        let detector = DeviceDetector::new(PathBuf::from(temp_dir.path()));

        // Create compatible file with a two machine pattern buffer
        let result = helper_create_compatible_file(temp_dir.path(), b"foo\0bar");
        assert!(result.is_ok());

        // Read the compatible info using device detector
        let result = detector.read_compatible_info().expect("Test failure");
        assert_eq!(result, vec!["foo", "bar"]);

        // Clean temp directory
        let _ = temp_dir.close();
    }

    #[test]
    fn read_compatible_info_error() {
        // Create a temp directory
        let temp_dir = tempdir().unwrap();

        // Create a device detector instance
        let detector = DeviceDetector::new(PathBuf::from(temp_dir.path()));

        // Read the compatible info using device detector when no compatible file exists
        let result = detector.read_compatible_info();
        assert!(result.is_err());

        // Clean temp directory
        let _ = temp_dir.close();
    }

    #[test]
    fn find_config_nominal() {
        // Create a temp directory
        let temp_dir = tempdir().unwrap();

        // Create a device detector instance
        let detector = DeviceDetector::new(PathBuf::from(temp_dir.path()));

        // Create compatible file with a two machine pattern buffer
        let result = helper_create_compatible_file(temp_dir.path(), b"foo\0bar");
        assert!(result.is_ok());

        // Create config directory with two configs
        let result = helper_create_config_files(temp_dir.path(), vec!["bar", "def"]);
        assert!(result.is_ok());

        // Read the compatible info using device detector
        let result = detector.find_config().expect("Test failure");
        assert_eq!(result.as_str(), "bar");

        // Clean temp directory
        let _ = temp_dir.close();
    }

    #[test]
    fn find_config_error_no_match() {
        // Create a temp directory
        let temp_dir = tempdir().unwrap();

        // Create a device detector instance
        let detector = DeviceDetector::new(PathBuf::from(temp_dir.path()));

        // Create compatible file with a two machine pattern buffer
        let result = helper_create_compatible_file(temp_dir.path(), b"foo\0bar");
        assert!(result.is_ok());

        // Create config directory with two configs
        let result = helper_create_config_files(temp_dir.path(), vec!["dead", "def"]);
        assert!(result.is_ok());

        // Read the compatible info using device detector
        let result = detector.find_config();
        assert!(result.is_err());

        // Clean temp directory
        let _ = temp_dir.close();
    }

    #[test]
    fn find_config_error_no_config() {
        // Create a temp directory
        let temp_dir = tempdir().unwrap();

        // Create a device detector instance
        let detector = DeviceDetector::new(PathBuf::from(temp_dir.path()));

        // Create compatible file with a two machine pattern buffer
        let result = helper_create_compatible_file(temp_dir.path(), b"foo\0bar");
        assert!(result.is_ok());

        // Create an empty config directory
        let result = fs::create_dir_all(temp_dir.path().join("etc/mobile-tweaks/configs"));
        assert!(result.is_ok());

        // Read the compatible info using device detector
        let result = detector.find_config();
        assert!(result.is_err());

        // Clean temp directory
        let _ = temp_dir.close();
    }
}
